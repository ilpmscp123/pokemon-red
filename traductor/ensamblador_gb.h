#pragma once

/*
   archivo: ensamblador_gb.h

   objetivo:

	Almacenar toda la información relativa a las
	instrucciones para el procesador del gameboy. 
*/

struct instruccion
{
  char *mnemonico;	//< cadena que lo representa.
  unsigned short int ciclos;	//< número de ciclos de reloj que consume.
  unsigned short int argumentos;//< número de argumentos para formar el mnemónico final.
  unsigned short int octetos;	//< número de octetos que forman la instrucción.
  unsigned char tipo_argumento; //< indica el tipo de argumento que recibe. 
};

