#include <stdio.h>

int  main(int n, char **args)
{
 FILE *fp = fopen( *(args+1),"r");
 unsigned char dato = 0;

 unsigned long int repeticiones[256];
 int i=0;

 for(i=0;i<256;i++)
 {
   repeticiones[i]=0;
 }

 
 while(!feof(fp))
 {
   fread(&dato,1,1,fp);
   repeticiones[dato]++;
 }
  

 for(i=0;i<256;i++)
 {
   printf("[%02x] = %ld\n", i, repeticiones[i] );
 }
 

}
