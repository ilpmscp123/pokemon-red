/*
  Traducción al lenguaje C del juego Pokemon Red.

  por Rafael Norman Saucedo Delgado.

*/

struct registros
{
  unsigned char A;
  unsigned char B;
};

/*
  Constante que indica que el sistema es un gameboy color.
*/
const int GAME_BOY_COLOR = 0x11;


int main(void)
{
  struct registros reg;

  /* 
    La primera instrucción en la posición 150h es 

	FEh que significa: CP - Comparar.

	FE 11
	compara con el valor 11h que significa gameboy color.
  */

   // verifica si el programa es para gameboy color
   if( reg.A == GAME_BOY_COLOR )
   {

   }



  return 0;
}
